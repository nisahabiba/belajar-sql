1.
CREATE DATABASE myshop;

2. 
Table Users :
CREATE TABLE users( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255) );

Table Categories :
CREATE TABLE categories( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) );

Table Items :
CREATE TABLE items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), description varchar(255), price int(11), stock int(11), category_id int(8), FOREIGN KEY(category_id) REFERENCES categories(id) );

3.
Table Users:
INSERT INTO users(name, email, password) VALUES ("John Doe", "john@doe.com", "john123");
INSERT INTO users(name, email, password) VALUES ("Jane Doe", "jane@doe.com", "jenita123");

Table Categories :
INSERT INTO categories(name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

Table Items :
INSERT INTO items(name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, "1"), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, "2"), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, "1");

4.
Mengambil data items:
SELECT id, name, email FROM users;

Menampilkan data items:
SELECT * FROM items WHERE price > 1000000;
SELECT * FROM items
WHERE name LIKE '%watch%'; 


Menampilkan data items join dengan kategori:
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS category FROM items INNER JOIN categories ON items.category_id=categories.id;

5.
UPDATE items SET price = 2500000 WHERE name = "Sumsang b50";